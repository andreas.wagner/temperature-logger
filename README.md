This repo contains three programs:

* temp-logger, a program which collects data from i.e. /dev/ttyUSB0 in newline-separated plaintext.
  - logs as 64bit timestamp + 64bit value.
* temp-log-eval, a program which creates .PNGs from the logged data.
  - Interprets data from temp-logger as LM35-readings, using the internal reference-voltage of 1.1V.
* sketch-apr16a.ino, a program for an "Ardiuno Uno"- and compatible-board, which is connected to a LM35-temperature-sensor on A0.
  - Reads temperatures from 0°C to 100°C.
  - Uses internal reference-voltage of 1.1V.
  - Flattens the signal using 128 reading-samples, summing them up and deviding it by 128, giving the average of the signal.
  - Sends data after connecting and when the value read changes by at least 1 after flattening the signal.
