#include <fstream>
#include <string>
#include <sys/time.h>
#include <cstring>
#include <iostream>
#include <ctime>

std::ifstream input;
std::ofstream output;

std::string timestamp(std::uint64_t seconds_since_1970)
{
  std::string ret("strftime failed");
  std::time_t t = seconds_since_1970;
    char mbstr[512];
    if (std::strftime(mbstr, sizeof(mbstr), "%FT%TZ%z", std::localtime(&t))) {
        ret = mbstr;
    }
  return ret;
}

std::string current_day(std::uint64_t seconds_since_1970)
{
  std::string ret("strftime failed");
  std::time_t t = seconds_since_1970;
    char mbstr[512];
    if (std::strftime(mbstr, sizeof(mbstr), "%F", std::localtime(&t))) {
        ret = mbstr;
    }
  return ret;
}

int main(int argc, char *argv[])
{
  struct timeval tv;
  bool switch_file_on_day_change = false;
  std::string last_date = current_day(time(NULL));
  
  
  if (argc == 3)
  {
    input.open(argv[1], std::ios::in);

    output.open(argv[2], std::ios::out | std::ofstream::binary);
  }
  else if (argc == 2)
  {
    switch_file_on_day_change = true;
    input.open(argv[1], std::ios::in);

    output.open(timestamp(time(NULL)), std::ios::out | std::ofstream::binary);
  }
  else
  {  
    std::cerr << argv[0] << " device [log-file]" << std::endl;
    return 1;
  }
  while(true)
  {
    std::string s;
    std::uint64_t reading;
    if(input.good())
    {
      bool worked = false;
      while(!worked)
      {
        input >> s;
        worked = true;
        try
        {
          reading = std::stoi(s);
        }
        catch(std::invalid_argument &e)
        {
          worked = false;
        }
      }
      
      if(true)
      {
        if(gettimeofday(&tv, nullptr) != 0)
        {
          std::cerr << "Error in gettimeofday()!" << std::endl;
        }
        std::uint64_t time = tv.tv_sec;
        time *= 1000000;
        time += tv.tv_usec;
        if(switch_file_on_day_change && last_date != current_day(tv.tv_sec))
        {
          last_date = current_day(tv.tv_sec);
          output.close();
          output.open(timestamp(tv.tv_sec), std::ios::out | std::ofstream::binary);
        }
        char test[sizeof(time) + sizeof(reading)];
        std::memcpy(test, &time, sizeof(time));
        std::memcpy(test+sizeof(time), &reading, sizeof(reading));
        output.write(test, sizeof(time) + sizeof(reading));
        output.flush();
      }
    }
  }
  return 0;

}
