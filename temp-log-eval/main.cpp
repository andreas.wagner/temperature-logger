#include <iostream>
#include <filesystem>
#include <ctime>
#include <fstream>
#include <cairo/cairo.h>
#include <cairo/cairo-pdf.h>


int main(int argc, char *argv[])
{
  double factor = 2.54; // inch / cm
  factor *= 1.0/72.0; // pt / inch -> pt/cm
  factor = 1 / factor;
  factor = factor * 3;
  
  double pt_max_x = 29.7 * factor;
  double pt_max_y = 21.0 * factor;
  double upper_marg = 0.5 * factor;
  double left_marg = 0.5 * factor;
  double right_marg = pt_max_x - 0.5 * factor;
  double lower_marg = pt_max_y - 0.5 * factor;
  
  double height = lower_marg - upper_marg;
  double width = right_marg - left_marg;
  
  double max_value = 1023;
  
  if(argc == 2)
  {
    std::ifstream input(argv[1]);
    cairo_surface_t *surface = nullptr;
    int last_day = -1;
    cairo_t *context = nullptr;
    std::uint64_t end_of_day_us = 0;
    std::string filename;
    
    
    while(input.good())
    {
      std::uint64_t time, reading;
      char buf[sizeof(time) + sizeof(reading)];
      input.read(buf, sizeof(time) + sizeof(reading));
      time = *((std::uint64_t*) buf);
      reading = *((std::uint64_t *)(buf + sizeof(time)));
      
      std::time_t tv = time/1000000;
      std::tm *tm = localtime(&tv);
      //std::cout << tv << ": " << reading << std::endl;
      
      if(last_day != tm->tm_mday)
      {
        last_day = tm->tm_mday;
        if(context != nullptr)
        {
          cairo_stroke(context);
          //cairo_show_page(context);
          cairo_surface_write_to_png(surface, filename.c_str());
          cairo_destroy(context);
        }
        if(surface != nullptr)
        {
          cairo_surface_destroy(surface);
        }
        filename = std::to_string(tm->tm_year+1900) + "-"
          + std::to_string(tm->tm_mon+1) + "-"
          + std::to_string(tm->tm_mday) + ".png";
        int filenumber = 0; 
        while(std::filesystem::exists(filename))
        {
          filename = std::to_string(tm->tm_year+1900) + "-"
          + std::to_string(tm->tm_mon+1) + "-"
          + std::to_string(tm->tm_mday) + "_" + std::to_string(filenumber) + ".png";
        }
        std::cout << filename << std::endl;
        surface = cairo_image_surface_create(CAIRO_FORMAT_ARGB32, pt_max_x, pt_max_y);
        context = cairo_create(surface);
        //cairo_save(context);
        /*cairo_rectangle(context, left_marg, upper_marg, width, height);
        cairo_set_source_rgb(context, 0, 0, 1);
        cairo_fill(context);*/
        //cairo_restore(context);
        
        cairo_select_font_face(context, "DejaVuSans", CAIRO_FONT_SLANT_NORMAL, CAIRO_FONT_WEIGHT_NORMAL);
        cairo_set_font_size(context, 0.4 * factor);

        cairo_text_extents_t text_extents;
        cairo_text_extents(context, filename.c_str(), &text_extents);
        cairo_set_source_rgb(context, 0, 0, 0);
        cairo_move_to(context,
          left_marg + width / 2 - text_extents.width / 2 - text_extents.x_bearing,
          upper_marg / 2 - text_extents.height / 2 - text_extents.y_bearing);
        cairo_show_text(context, filename.c_str());
        
        cairo_set_line_join(context, CAIRO_LINE_JOIN_ROUND);
        cairo_set_line_cap(context, CAIRO_LINE_CAP_ROUND);
        cairo_set_line_width(context, 0.03 * factor);
        cairo_set_source_rgb(context, 1, 0, 0);
        
        for(int i = 0; i < 10; i++)
        {
          double y = lower_marg - i * (110.0/1023.0) * height;
          cairo_new_path(context);
          cairo_line_to(context, left_marg, y);
          cairo_line_to(context, right_marg, y);
          cairo_stroke(context);
          cairo_move_to(context, left_marg + 0.05 * factor, y + 0.4125 * factor);
          cairo_show_text(context, std::string(std::to_string(i * 10) + " °C").c_str());
        }
        cairo_set_line_width(context, 0.03 * factor);
        cairo_set_source_rgb(context, 0, 0, 1);
        for(int i = 0; i < 12; i++)
        {
          double x = i * width / 12.0 + left_marg;
          cairo_new_path(context);
          cairo_line_to(context, x, upper_marg);
          cairo_line_to(context, x, lower_marg);
          cairo_stroke(context);
          cairo_move_to(context, x + 0.05 * factor, upper_marg + 0.25 * factor);
          cairo_show_text(context, std::string(std::to_string(i * 2) + " Uhr").c_str());
        }
        cairo_new_path(context);
        cairo_set_line_width(context, 0.025 * factor);
        cairo_set_source_rgb(context, 0, 0, 0);
      }
      
      double x = tm->tm_hour*60*60 + tm->tm_min*60 + tm->tm_sec;
      x += (time % 1000000)/1000000.0;
      x *= width;
      x /= 24*60*60;
      
      double y = lower_marg - reading * height / max_value;
      
      cairo_line_to(context, x + left_marg, y + upper_marg);
      //cairo_paint(context);
    } // while(input.good())
    
    if(context != nullptr)
    {
      cairo_stroke(context);
      //cairo_show_page(context);
      cairo_surface_write_to_png(surface, filename.c_str());
      cairo_destroy(context);
    }
    if(surface != nullptr)
    {
      cairo_surface_destroy(surface);
    }
  }
}
