/* LM35 analog temperature sensor with Arduino example code. More info: https://www.makerguides.com */

// Define to which pin of the Arduino the output of the LM35 is connected:
#define sensorPin A0
#define filterpoints 128

int last_readings[filterpoints];
long last_sum;

bool serial_is_available = false;
void setup() {
  // Begin serial communication at a baud rate of 9600:
  Serial.begin(9600);

  // Set the reference voltage for analog input to the built-in 1.1 V reference:
  analogReference(INTERNAL);
  for(int k = 0; k < filterpoints; k++)
  {
    last_readings[k] = analogRead(sensorPin);
    last_sum += last_readings[k];
  }
}

int i = 0;
void loop() {
  // Get a reading from the temperature sensor:
  
  int reading = analogRead(sensorPin);
  last_readings[i] = reading;
  i++; i %= filterpoints;

  long sum = 0;
  for(int j = 0; j < filterpoints; j++)
  {
    sum += last_readings[j];
  }

  if(Serial.availableForWrite())
  {
    if(!serial_is_available || !(abs(last_sum - sum)/filterpoints < 2))
    {
      serial_is_available = true;
      Serial.print( sum / filterpoints);
      Serial.print("\n");
      Serial.flush();
      last_sum = sum;
    }
  }
  else // !Serial.availableForWrite()
  {
    serial_is_available = false;
  }
/*
  // Convert the reading into voltage:
  float voltage = reading * (1100 / 1024.0);

  // Convert the voltage into the temperature in degree Celsius:
  float temperature = voltage / 10;

  // Print the temperature in the Serial Monitor:
  Serial.print(temperature);
  Serial.print(" \xC2\xB0"); // shows degree symbol
  Serial.println("C");

  delay(1000); // wait a second between readings
  */
}
