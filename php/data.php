<!DOCTYPE html>
<html lang="de">
<head>
<title>Temperature-Logger</title>
</head>
<body>
<h1>Temperature-Logger</h1>
<?php
$myList = array();
foreach (new DirectoryIterator('/srv/log_data') as $fileInfo) {
  array_push($myList, $fileInfo->getFilename());
}
sort($myList);
foreach ($myList as $fileInfo) {
  if(preg_match('/^\..*/', $fileInfo)) continue;
  if(preg_match('/.*\.png/', $fileInfo) )
  {
    echo '<a href="/data/' . urlencode($fileInfo) . '">' . $fileInfo . "</a>";
    echo ' <a href="delete.php?rm=' . urlencode($fileInfo) . '">Löschen</a><br>' . "\n";
  }
  else
    echo $fileInfo . ' <a href="create_visualisation.php?t=png&v=' . urlencode($fileInfo) . '">create .PNG</a><br>'."\n";
}
?>
</body>
</html>
